import java.util.Scanner;
public class Roulette {
    public static void main(String args[]) {
        Roulettewheel rWheel = new Roulettewheel();
        Scanner scan = new Scanner(System.in);
        int guess = 0;
        int ownedMoney = 1000;
        int betMoney = 0;
        System.out.println("Would you like to bet? (yes or no)");
        String answerToBet = scan.next();
        if (answerToBet.equalsIgnoreCase("yes")) {
            System.out.println("How much would you like to bet?");
            betMoney = scan.nextInt();
            if (betMoney <= 0 || betMoney > 1000) {
                do {
                    System.out.println("You have 1000$ max, you need to bet within that amount");
                    betMoney = scan.nextInt();
                } while (betMoney <= 0 || betMoney > 1000);
            }
            System.out.println("Which number would you like to bet on? (0-36)");
            guess = scan.nextInt();
            if (guess < 0 || guess > 36) {
                do {
                    System.out.println("You need to bet between 0 and 36");
                    guess = scan.nextInt();
                } while (guess < 0 || guess > 36);
            }
            rWheel.spin();
            int winningNumber = rWheel.getValue();
            if (isWinning(guess, winningNumber)) {
                System.out.println("You win!");
                ownedMoney = betMoney * 35 + ownedMoney ;
                System.out.println("You now have " + ownedMoney + "$ ");
            } else {
                System.out.println("You lose... It landed on " + winningNumber);
                ownedMoney = ownedMoney - betMoney;
                System.out.println("You now have " + ownedMoney + "$ ");
            }
        }
    }
    public static boolean isWinning(int guess, int winningNumber) {
        if (guess == winningNumber) {
            return true;
        }
        return false;
    }
}